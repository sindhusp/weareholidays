$(document).ready(function () {
	var artistnamearr = new Array();
    $("a[rel]").overlay({
        onBeforeLoad: function () {
            var wrap = this.getOverlay().find(".contentWrap");
            wrap.load(this.getTrigger().attr("href"), function(response, status, xhr) {
				if (status == "error") {
					var msg = "Sorry but there was an error: ";
					$("#error").html(msg + xhr.status + " " + xhr.statusText);
					console.log(xhr.status);
					console.log(xhr.statusText);
				}
			});
        },
		onLoad: function () {
                $('.contentWrap form').submit(function (event) {
                    event.preventDefault();
					val = validate('artist');
					if (val) {
						submit_info('artist');
					}
                });
            }
	});		
	function closeOverlay() {
                    $("a[rel]").overlay().close();
					}
	function validate(name) {
					var artistName=document.forms[name]["artistname"].value;
					var no_of_tracks=document.forms[name]["No_of_tracks"].value;
					if (!((artistName=="jack") || (artistName=="Jack"))) {
						alert("Fill the right name");
					  }
					else if(!(no_of_tracks==4))  {
						alert("Fill the  right number of tracks");
					 }
					else return true;
	}
	function submit_info(name) {
					var artistName=document.forms[name]["artistname"].value;
					var no_of_tracks=document.forms[name]["No_of_tracks"].value;
					var url="http://itunes.apple.com/search?term=" + artistName + "&limit=" + no_of_tracks;
					$.ajax({
						url: url,
						dataType: 'jsonp',
						success: function(data) {
							closeOverlay();
							$.each(data["results"], function(i, item) {
								createTab(item,i);
							});
						}
					});
					}			
			
	function createTab(item,i) {
		var artistName = item["artistName"];
		//artistName= artistName.replace(/ /g,"+"); Tab shows clean name
		var str = "<li class='pane' ><a href='#pane'"+i+" id='#pane"+i+"' data-toggle='tab'>" + artistName +"</li>";
		$("#tablist").append(str);
		//var div = '<div id="#pane'+i+'"></div>';
	}
	function replace(text) {
		text= text.replace(/ /g,"+");
		return text;
	}
	$( '#tabs' ).on( 'click', 'a', function () { 
				var artistName = this.text;
				var id=this.id;
				var url="http://itunes.apple.com/search?term=" + this.text + "&limit=1";
				url = replace(url);
				$.ajax({
						url: url,
						dataType: 'jsonp',
						success: function(data) {
							$.each(data["results"], function(i, item) {
								//populatePane(item,i);
								html = "<div id='"+id+"'> <p> The name of this artist is "+item["artistName"]+" and the name of the track is "+item['trackName']+". For info about their artwork, click <a href='"+item['artworkUrl30']+"' >this link </a></p></div>";
								$('#panes').html(html);
							});
						}
				});	
	});
});
